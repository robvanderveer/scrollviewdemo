# ScrollViewDemo 
## Fixing blocky shapes

This demo shows how to use the `UIScrollView` to properly scroll and zoom a `CAShapeLayer`.
When the shape is zoomed in (to 8x the size in this demo) the pie part remains super sharp and does not go blocky.

## Why does it go blocky in the first place?

It goes blocky because the the layer has `ShouldRasterize` set to `YES` and the `rasterizationScale` property of the sublayer is not adjusted.

## The trick
When zooming in, we must retrigger the rasterization for the proper zoom factor.

- The trick is to multiply the `[UIScrollView scale]` property with the `[[UIScreen mainscreen] scale]` value first.
This first multiplication gives a proper scale value for retina and non-retina devices.

- Then we set the `contentScale` and `rasterizationScale` to that value for each sublayer. 

Changing the `rasterizationScale` will retrigger the rasterization. It still uses the buffered version in animations, but in crystal clear sharpness! 

Basically, here's the code:

	-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
	{
    	NSLog(@"scale: %f",scale);
    
    	scale *= [[UIScreen mainScreen] scale];
    	for(CALayer *sublayer in _scrollContents.layer.sublayers)
  		{
        	sublayer.rasterizationScale = scale;
        	sublayer.contentsScale = scale;
    	}
	}
	
The `contentsScale` is optional if you have only `CAShapeLayers`. Please note that the following things did NOT fix it:

- Calling `[self updateSubLayers]`
- Calling `[self display]`
- Changing the size of the content and doing intricate sizing calculations
- Drinking more coffee.

Check out my blog <http://simplicate.weebly.com>.