//
//  ViewController.h
//  ScrollViewDemo
//
//  Created by Rob van der Veer on 9/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewer;
@property (strong, nonatomic) IBOutlet UIView *scrollContents;
- (IBAction)fixChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *fixMeSelector;

@end
