//
//  ViewController.m
//  ScrollViewDemo
//
//  Created by Rob van der Veer on 9/6/13.
//  Copyright (c) 2013 Rob van der Veer. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()
@property (nonatomic) CGFloat contentScale;
@end

#define CLAMP(a,low,max) MAX(low, MIN(max, a));


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 500, 300)];
    [containerView.layer setBackgroundColor:[UIColor orangeColor].CGColor];
    [containerView.layer setCornerRadius:20];
    
    //add a piePath for testing.
    [containerView.layer addSublayer:[self createShape:containerView.bounds.size]];
    
    _scrollContents = containerView;
    
    [_scrollViewer addSubview:_scrollContents];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self updateZoomScale:false];
}

- (void)centerView:(UIView *)view inSize:(CGSize)boundsSize
{
    //while we're at it, center the content.
    CGRect frameToCenter = view.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;
    
    view.frame = frameToCenter;
}

//Returns the zoom scale
-(CGFloat)updateZoomScale:(bool)keepExisting
{
    //now we know the size of the containerView, we can determine the scale factor.
    CGSize contentSize = _scrollContents.bounds.size;
    
    CGFloat xScale = _scrollViewer.bounds.size.width / contentSize.width;
    CGFloat yScale = _scrollViewer.bounds.size.height / contentSize.height;
    CGFloat scale = MIN(xScale, yScale);
    
    _scrollViewer.minimumZoomScale = scale;
    _scrollViewer.maximumZoomScale = 8;
    if(keepExisting)
    {
        //limit the current scale between min and max.
        scale = CLAMP(_scrollViewer.zoomScale, scale, 1);
    }
    
    [UIView animateWithDuration:0.5 delay:0
                        options:UIViewAnimationOptionLayoutSubviews
                     animations:^{
                         _scrollViewer.zoomScale = scale;
                         // layout the subviews here
                     } completion:^(BOOL finished) {}];
    
    [self centerView:_scrollContents inSize:_scrollViewer.bounds.size];
    return scale;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return _scrollContents;
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    NSLog(@"scale: %f",scale);
    
    if(_fixMeSelector.selectedSegmentIndex == 1)
    {
        scale *= [[UIScreen mainScreen] scale];
        for(CALayer *sublayer in _scrollContents.layer.sublayers)
        {
            sublayer.rasterizationScale = scale;
            sublayer.contentsScale = scale;
        }
    }
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self updateZoomScale:true];
}

-(CAShapeLayer *)createShape:(CGSize)size
{
    CAShapeLayer *shape = [CAShapeLayer layer];
    
    
    CGPoint middle = CGPointMake(size.width/2, size.height/2);
    CGFloat radius = size.height/3;
    CGFloat start = 0.3;
    CGFloat end = 2;
    
    [shape setLineWidth:3];
    [shape setFillColor:[[UIColor alloc] initWithWhite:1.0 alpha:0.3].CGColor];
    [shape setStrokeColor:[UIColor whiteColor].CGColor];
    [shape setShouldRasterize:YES];
    [shape setRasterizationScale:[[UIScreen mainScreen] scale]];
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, middle.x, middle.y);
    CGPathAddLineToPoint(path, NULL, middle.x + radius * cosf(start),
                         middle.y + radius * sinf(start));
    CGPathAddArc(path, NULL, middle.x, middle.y, radius, start, end, true);
    CGPathAddLineToPoint(path, NULL, middle.x, middle.y);
    CGPathCloseSubpath(path);
    
    [shape setPath:path];
    
    return shape;
}

- (IBAction)fixChanged:(id)sender
{
    CGFloat scale = [[UIScreen mainScreen] scale];
    if(_fixMeSelector.selectedSegmentIndex == 1)
    {
        scale *= _scrollViewer.zoomScale;
    }
    
    for(CALayer *sublayer in _scrollContents.layer.sublayers)
    {
        sublayer.rasterizationScale = scale;
        sublayer.contentsScale = scale;
    }

}
@end
